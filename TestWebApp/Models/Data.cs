﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace TestWebApp.Models
{
    [Table("tbl_test", Schema = "public")]
    public class Data
    {
        [Key]
        public int txt_id { get; set; }
        public string txt_data { get; set; }
    }
}