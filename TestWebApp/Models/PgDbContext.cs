﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace TestWebApp.Models
{
    public class PgDbContext: DbContext
    {
        public PgDbContext(): base(nameOrConnectionString: "Default") { }
        public virtual DbSet<Data> Dt { get; set; }
    }
}