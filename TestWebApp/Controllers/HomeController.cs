﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using TestWebApp.Models;

namespace TestWebApp.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            PgDbContext entities = new PgDbContext();
            List<Data> data = entities.Dt.ToList();
            return View(data);
        }

        public JsonResult InsertData(Data data)
        {
            using (PgDbContext entities = new PgDbContext())
            {
                entities.Dt.Add(data);
                entities.SaveChanges();
            }
            return Json(data);
        }

        public ActionResult UpdateData(Data data)
        {
            using(PgDbContext entities = new PgDbContext())
            {
                Data updateData = (from c in entities.Dt where c.txt_id == data.txt_id select c).FirstOrDefault();
                updateData.txt_data = data.txt_data;
                entities.SaveChanges();
            }
            return new EmptyResult();
        }

        public ActionResult DeleteData(int dataId)
        {
            using(PgDbContext entities = new PgDbContext())
            {
                Data data = (from c in entities.Dt where c.txt_id == dataId select c).FirstOrDefault();
                entities.Dt.Remove(data);
                entities.SaveChanges();
            }
            return new EmptyResult();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }
    }
}