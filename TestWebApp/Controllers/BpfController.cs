﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace TestWebApp.Controllers
{
    public class BpfController : Controller
    {
        // GET: Bpf
        public ActionResult Index()
        {
            return View();
        }
        //next copy of code from another project
        List<double> xVal = new List<double>();
        List<double> yVal = new List<double>();
        public double a, b, c, x2, x1, x0;
        public static int N;
        public double[] points = new double[N];
        public double[] satFilter(double a, double b, double x1, double x0)
        {
            double p;
            double[] points = new double[N];
            points[0] = x0;
            points[1] = x1;
            for (int i = 2; i < points.Length; i++)
            {
                p = a * points[i - 1] + b * points[i - 2];
                if (p > 1)
                {
                    points[i] = 1;
                }
                else if (p < 1)
                {
                    points[i] = -1;
                }
                else
                {
                    points[i] = p;
                }
            }
            return points;
        }
        public double[] zerFilter(double a, double b, double x1, double x0)
        {
            double p;
            double[] points = new double[N];
            points[0] = x0;
            points[1] = x1;
            for (int i = 2; i < points.Length; i++)
            {
                p = a * points[i - 1] + b * points[i - 2];
                if (Math.Abs(p) <= 1)
                {
                    points[i] = p;
                }
                if (p > 1)
                {
                    points[i] = 0;
                }
            }
            return points;
        }
        public double[] newFilter(double a, double b, double x1, double x0)
        {
            double p;
            double[] points = new double[N];
            points[0] = x0;
            points[1] = x1;
            for (int i = 2; i < points.Length; i++)
            {
                p = a * points[i - 1] + b * points[i - 2];
                if (p > 1)
                {
                    while (p > 1)
                    {
                        p = p - 2;
                    }
                    points[i] = p;
                }
                else
                {
                    while (p < -1)
                    {
                        p = p + 2;
                    }
                    points[i] = p;
                }
            }
            return points;
        }
        private double ConvertToDouble(string s)
        {
            char systemSeparator = Thread.CurrentThread.CurrentCulture.NumberFormat.CurrencyDecimalSeparator[0];
            double result = 0;
            try
            {
                if (s != null)
                    if (!s.Contains(","))
                        result = double.Parse(s, CultureInfo.InvariantCulture);
                    else
                        result = Convert.ToDouble(s.Replace(".", systemSeparator.ToString()).Replace(",", systemSeparator.ToString()));
            }
            catch
            {
                try
                {
                    result = Convert.ToDouble(s);
                }
                catch
                {
                    try
                    {
                        result = Convert.ToDouble(s.Replace(",", ";").Replace(".", ",").Replace(";", "."));
                    }
                    catch
                    {
                        throw new Exception("Wrong string-to-double format:");
                    }
                }
            }
            return result;
        }
        public double[] satFilterIncludeX2(double a, double b, double x2, double x1, double x0)
        {
            double p;
            double[] points = new double[N];
            points[0] = x0;
            points[1] = x1;
            points[2] = x2;
            for (int i = 3; i < points.Length; i++)
            {
                p = a * points[i - 1] + b * points[i - 2] + c * points[i - 3];
                if (p > 1)
                {
                    points[i] = 1;
                }
                else if (p < 1)
                {
                    points[i] = -1;
                }
                else
                {
                    points[i] = p;
                }
            }
            return points;
        }
        public double[] zerFilterIncludeX2(double a, double b, double x2, double x1, double x0)
        {
            double p;
            double[] points = new double[N];
            points[0] = x0;
            points[1] = x1;
            points[2] = x2;
            for (int i = 3; i < points.Length; i++)
            {
                p = a * points[i - 1] + b * points[i - 2] + c * points[i - 3];
                if (Math.Abs(p) <= 1)
                {
                    points[i] = p;
                }
                if (p > 1)
                {
                    points[i] = 0;
                }
            }
            return points;
        }
        public double[] newFilterIncludeX2(double a, double b, double x2, double x1, double x0)
        {
            double p;
            double[] points = new double[N];
            points[0] = x0;
            points[1] = x1;
            points[2] = x2;
            for (int i = 3; i < points.Length; i++)
            {
                p = a * points[i - 1] + b * points[i - 2] + c * points[i - 3];
                if (p > 1)
                {
                    while (p > 1)
                    {
                        p = p - 2;
                    }
                    points[i] = p;
                }
                else
                {
                    while (p < -1)
                    {
                        p = p + 2;
                    }
                    points[i] = p;
                }
            }
            return points;
        }
    }
}